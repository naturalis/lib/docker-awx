# docker-awx

Deploy AWX using AWX installer and docker-compose

    git clone -b 9.0.1 https://github.com/ansible/awx
    cd awx/installer

Edit inventory file:
    
    postgres_data_dir="/data/pgdata"
    docker_compose_dir="/opt/composeproject"
    pg_password=***
    pg_admin_password=***
    rabbitmq_password=***
    admin_password=***
    secret_key=***

Install packages:

    apt install python-pip
    pip install docker-compose
    install docker

Run the ansible playbook:

    ansible-playbook -i inventory install.yml

Edit /opt/composeproject/docker-compose.yml:

    Set file version from 2 to 3.4
    
    Add code block:
    
    X-logging:
      &default-logging
      options:
        max-size: '10m'
        max-file: '5'
    driver: json-file

    Add to every service:
    
    logging: *default-logging
    
Create .env file and add:

    AWS_ACCESS_KEY_ID: ***
    AWS_SECRET_ACCESS_KEY: ***
    AWS_HOSTED_ZONE_ID: ***
    
Add traefik code block:

    traefik:
      image: traefik:2.0.5
      restart: unless-stopped
      environment:
        - AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
        - AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
        - AWS_HOSTED_ZONE_ID=${AWS_HOSTED_ZONE_ID}
      ports:
        - 80:80
        - 443:443
        - 8081:8080
      volumes:
        - /var/run/docker.sock:/var/run/docker.sock
        - ./traefik:/etc/traefik
      logging: *default-logging

Remove ports definition in awx_web.

Create folder traefik.
Add files traefik.toml and dynamic-conf.toml from this repo.